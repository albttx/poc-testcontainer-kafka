module myapp

go 1.15

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/pkg/errors v0.9.1
	github.com/segmentio/kafka-go v0.4.8
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/testcontainers/testcontainers-go v0.9.0
)
