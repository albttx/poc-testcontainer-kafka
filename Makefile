
build:
	go build -o ./bin/ ./cmd/producer
	go build -o ./bin/ ./cmd/consumer

test:
	go test -v ./...
