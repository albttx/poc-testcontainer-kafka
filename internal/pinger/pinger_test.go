package pinger_test

import (
	"fmt"
	"myapp/internal/pinger"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	tc "github.com/testcontainers/testcontainers-go"
)

var (
	composeFilePaths = []string{"../../docker-compose.yml"}

	pingerService pinger.Service
)

func init() {
	if os.Getenv("DEBUG") != "" {
		logrus.SetLevel(logrus.DebugLevel)
	}
}

func TestMain(m *testing.M) {
	var err error

	identifier := strings.ToLower(uuid.New().String())

	if _, err := os.Stat(composeFilePaths[0]); os.IsNotExist(err) {
		logrus.Fatal("docker-compose file not found")
	}

	// testcontainer start kafka doker-compose
	logrus.Info("Starting kafka docker-compose")

	compose := tc.NewLocalDockerCompose(composeFilePaths, identifier)
	err = compose.WithCommand([]string{"up", "-d"}).Invoke().Error
	if err != nil {
		logrus.WithError(err).Fatalf("Could not run compose file: %v", composeFilePaths)
	}

	logrus.Info("Wait 5 seconds for docker service to be up")
	time.Sleep(time.Second * 5)
	logrus.Info("Sleep done")

	defer func() {
		logrus.Info("Down service")
		if err := compose.Down().Error; err != nil {
			logrus.WithError(err).Error()
		}
	}()

	// Create new pinger service
	pingerService, err = pinger.NewService(pinger.Config{
		KafkaURI:  "localhost:9092",
		Topic:     "test",
		Partition: 0,
	})
	if err != nil {
		logrus.WithError(err).Fatal()
	}
	defer pingerService.Close()

	os.Exit(m.Run())
}

func TestSendMessage(t *testing.T) {
	chMsg := make(chan string)
	defer close(chMsg)

	go func() {
		err := pingerService.RecvToChan(chMsg)
		require.Nil(t, err)
	}()

	data := make(map[string]bool)

	for i := 0; i < 5; i++ {
		randomUUID := fmt.Sprintf("Merry Christmas %v", i)
		data[randomUUID] = false

		err := pingerService.SendMessage(randomUUID)
		require.Nil(t, err)
	}

	i := 0
	for msg := range chMsg {
		if _, exist := data[msg]; exist {
			data[msg] = true
		}
		logrus.WithField("msg", msg).Debug()

		i++

		if i == 5 {
			break
		}
	}
	for k, v := range data {
		if v == false {
			logrus.Errorf("Key %v should have been receive with value %v", k, v)
		}
	}
}
