package pinger

import (
	"context"
	"strings"

	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

// Service interface for pinger
type Service interface {
	SendMessage(msg string) error
	Recv() error
	RecvToChan(chMsg chan string) error

	Close() error
}

// Client for pinger is complient to Service interface
type Client struct {
	cfg Config

	conn *kafka.Conn
}

// Config for pinger Client
type Config struct {
	KafkaURI  string
	Topic     string
	Partition int
}

// NewService return a Service compatible struct
func NewService(cfg Config) (Service, error) {
	c := &Client{
		cfg: cfg,
	}

	conn, err := kafka.DialLeader(context.Background(), "tcp", cfg.KafkaURI, cfg.Topic, cfg.Partition)
	if err != nil {
		return nil, errors.WithMessage(err, "failed to dial leader")
	}
	c.conn = conn

	return c, nil
}

// SendMessage ...
func (c Client) SendMessage(msg string) error {
	_, err := c.conn.WriteMessages(
		kafka.Message{Value: []byte(msg)},
	)
	return err
}

// Recv merry cristmas message
func (c Client) Recv() error {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  strings.Split(c.cfg.KafkaURI, ","),
		Topic:    c.cfg.Topic,
		MinBytes: 10,
		MaxBytes: 10e6,
	})
	defer reader.Close()

	for {
		msg, err := reader.ReadMessage(context.Background())
		if err != nil {
			return err
		}
		logrus.WithField("msg", string(msg.Value)).Info()
	}
}

// Recv merry cristmas message
func (c Client) RecvToChan(chMsg chan string) error {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  strings.Split(c.cfg.KafkaURI, ","),
		Topic:    c.cfg.Topic,
		MinBytes: 10,
		MaxBytes: 10e6,
	})
	defer reader.Close()

	for {
		msg, err := reader.ReadMessage(context.Background())
		if err != nil {
			return err
		}
		chMsg <- string(msg.Value)
	}
}

// Close the kafka connection
func (c Client) Close() error {
	return c.conn.Close()
}
