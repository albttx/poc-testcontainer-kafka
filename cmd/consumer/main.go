package main

import (
	"myapp/internal/pinger"

	"github.com/caarlos0/env"
	"github.com/sirupsen/logrus"
)

type config struct {
	Debug bool `env:"DEBUG" envDefault:"false"`

	KafkaURI string `env:"KAFKA_URI" envDefault:"localhost:9092"`
}

func main() {
	cfg := config{}

	if err := env.Parse(&cfg); err != nil {
		logrus.WithError(err).Fatal()
	}

	if cfg.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	pingerService, err := pinger.NewService(pinger.Config{
		KafkaURI:  cfg.KafkaURI,
		Topic:     "xmas",
		Partition: 0,
	})
	if err != nil {
		logrus.WithError(err).Fatal()
	}
	defer func() {
		if err := pingerService.Close(); err != nil {
			logrus.WithError(err).Error()
		}
	}()

	logrus.Info("Recv messages")
	if err := pingerService.Recv(); err != nil {
		logrus.WithError(err).Fatal()
	}
}
